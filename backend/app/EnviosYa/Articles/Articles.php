<?php

namespace EnviosYa\Articles;

use EnviosYa\Base\BaseEntity;

class Articles extends BaseEntity
{
    
protected $fillable = ['header_label','title','lead','body','slug','sections_id','users_id'];

protected $table = 'articles';

   	

}	
