<?php 
namespace EnviosYa\Articles;

interface ArticlesRepositoryInterface
{
    public function findOrFail($id);
    public function create(array $data);
    public function update($entity, array $data);
    public function delete($entity);



}