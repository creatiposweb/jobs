<?php 
namespace EnviosYa\Menu;

interface MenuRepositoryInterface
{
    public function findOrFail($id);
    public function create(array $data);
    public function update($entity, array $data);

    public function delete($entity);

    public function searchPartners($s);
    public function getcadeterias();
    public function getcomercios();
    public function getmenus($id);
    public function getUsers();
    public function generatePartnerMonthlyFees($entity);
    public function getPartnerPaymentsStatus($entity);


}