<?php

namespace EnviosYa\Ordenes;

use EnviosYa\Base\BaseRepository;
use EnviosYa\Payment\Payment;
use EnviosYa\User\Partner;
use EnviosYa\User\Profile;
use EnviosYa\User\RoleUser;
use EnviosYa\User\User;
use EnviosYa\Ordenes\Ordenes;
use Carbon\Carbon;
use File;
class OrdenesRepository extends BaseRepository implements OrdenesRepositoryInterface
{
    /**
     * @var User
     */
    protected $model;
    /**
     * UserRepository constructor.
     * @param User $model
     */
    public function __construct(Ordenes $model)
    {
        $this->model   = $model;
 
    }

    public function getModel()
    {
        return $this->model;
    }



        public function getorden($id)
    {
        
        return  $this->getModel()
                    ->join('menu', 'menu.id', '=', 'comandas.id_plato')
                    ->join('users', 'users.id', '=', 'comandas.id_user')
                    ->where('id_user2', '=', $id)

                    ->select('menu.id','menu.nombre as menu','users.name as comensal','comandas.created_at as fecha')
                    ->paginate(50);

    }

            public function getcomercios()
    {
        
        return  $this->getModel()
                    ->join('ordenes', 'ordenes.id_comercio', '=', 'users.id')
                    ->join('profiles', 'users.id', '=', 'profiles.user_id')
                    ->select('users.id','users.email','users.username','profiles.name','profiles.last_name','profiles.dni','profiles.phone','profiles.email_2','profiles.address','profiles.name_comercial','profiles.province_id','ordenes.id','ordenes.id_comercio','ordenes.id_cadeteria','ordenes.id_cadete','ordenes.direccion1','ordenes.direccion2','ordenes.estado','ordenes.descripcion','ordenes.precio','ordenes.created_at','ordenes.updated_at')
                    ->paginate(50);

    }


    /**
     * @overwrite method create
     * @param  array $data
     * @return Aatalac\User\User $user
     */
    public function create(array $data)
    {


        $data['password'] = bcrypt($data['password']);
        $user             = $this->model->create($data);
        $data['user_id']  = $user->id;
    
       
        $user->save();
        Profile::create($data);
        $data['role_id']  = $data['role_id'];
        RoleUser::create($data);
        return $user;
    }
    /**
     * @overwrite method update
     * @param  array $data
     * @return Aatalac\User\User $user
     */
    public function update($user, array $data)
    {

  
        
        // if(isset($data['plan_id'])){
        //     if($user->partner != null ){
        //         $user->partner->plan_id = $data['plan_id'];
        //     }
        // }
        // if(isset($data['title'])){
        //     $path = public_path().'/titles/';
        //     if(!\File::exists($path)){
        //         \File::makeDirectory($path);
        //     }
        //     $file = \File($data['title']);
        //     var_dump($file);
        //     dd("a");
        // }
        $user->fill($data);
        $user->profile->fill($data);
        $user->push();
        // if($user->partner){
        //     $user->partner->status = $data['status'];
        //     $user->partner->is_master = $data['is_master'];
        //     $user->partner->save();
        // }
        return $user;
    }


     



    /**
     * @overwrite method delete
     * @param  Aatalac\User\User $user
     * @return boolean
     */
    public function delete($user)
    {
        if (is_numeric($user)) {
            $user = $this->findOrFail($user);
        }
        if (count($user->commissions) <= 0) {
            $this->deleteImage($user->getPath(), $user->image);
            $user->delete();
            return true;
        }

        return false;
    }

    /**
     *
     * @param  array $data
     * @return Aatalac\User\User $user
     */
    public function findOrCreate(array $data)
    {
        $userAux         = $this->getModel()::where('email', $data['email'])->first();
        $data['plan_id'] = 0;
        if ($userAux == null) {
            $userAux         = $this->create($data);
            $data['user_id'] = $userAux->id;
        }

        return $userAux;
    }



    public function searchPartners($s = '')
    {
        
        return  $this->getModel()
                    ->join('profiles', 'users.id', '=', 'profiles.user_id')
                    ->join('partners', 'users.id', '=', 'partners.user_id')
                    ->join('plans', 'partners.plan_id', '=', 'plans.id')
                    ->where('users.rol', '=', 'partner')
                    ->where('users.email','like','%'.$s.'%')
                    ->orWhere('profiles.name','like','%'.$s.'%')
                    ->where('users.rol', '=', 'partner')
                    ->orWhere('profiles.last_name','like','%'.$s.'%')
                    ->where('users.rol', '=', 'partner')
                    ->orderBy("partners.id", "asc")
                    ->select('users.id','users.email','profiles.name','profiles.last_name','partners.id as partner_id','plans.name as plan_name')
                    ->paginate(50);

    }


    public function getUsers($s = '')
    {
        // return $this->getModel()->orderBy("created_at", "asc")->where('rol', '=', 'partner')->paginate(50);
        return  $this->getModel()
                    ->join('profiles', 'users.id', '=', 'profiles.user_id')
                    ->whereIn('users.rol',['user','pending_partner'])
                    ->where('users.email','like','%'.$s.'%')
                    ->orWhere('profiles.name','like','%'.$s.'%')
                    ->whereIn('users.rol',['user','pending_partner'])
                    ->orWhere('profiles.last_name','like','%'.$s.'%')
                    ->whereIn('users.rol',['user','pending_partner'])
                    ->orderBy("users.created_at", "asc")
                    ->orderBy("users.created_at", "asc")
                    ->select('users.id','users.email','profiles.name','profiles.last_name')
                    ->paginate(50);
        
    }


    public function translateMonth($num)
    {
        $num        = $num - 1;
        $arrSpanish = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];
        return $arrSpanish[$num];
    }

    public function generatePartnerMonthlyFees($user)
    {
        $arrFeesAux = [];
        if ($user->rol == 'partner' && isset($user->partner)) {
            /*Si es socio maestro no retorna vacio ya que no tiene que pagar cuotas*/
            if($user->partner->is_master=='1')
                return [];
            $createdAt = $user->partner->created_at;
            //$crearedAt = Carbon::parse($createdAt)->format('Y-m-d');
            $today = Carbon::today(); //->format('Y-m-d');

            $stop  = false;
            $date1 = $createdAt;
            $whileCount = 0;
            while ($stop == false) {
                $date2       = $date1->copy()->addMonth();
                $period      = $date1->format('d') . ' ' . $this->translateMonth($date1->format('m')) . ' ' . $date1->format('Y');
                $labelPeriod = $period;
                $paymentAux  = Payment::where('transaction_detail', $period)
                    ->where('user_id', $user->id)
                    ->where('partner_id', $user->partner->id)
                    ->where('payment_type', 'partner_fee')
                    ->orderBy('created_at', 'desc')
                    ->first();

                $status        = 'pending';
                $paymentDate   = '';
                $paymentAmount = '';
                // $bill_id = $paymentAux->bill_id;
                if ($paymentAux != null) {
                    if ($paymentAux->payment_status == 'approved') {
                        $status = 'paid';
                    }
                    $paymentDate     = Carbon::parse($paymentAux->created_at)->format('Y-m-d');
                    $paymentAmount   = $paymentAux->payment_amount;
                    $paymentPlanName = $paymentAux->plan->name;
                    $bill_id         = $paymentAux->bill_id;
                } else {
                    $paymentPlanName = "";
                    $bill_id         = "";
                    
                }

                //print_r($paymentAux->bill_id);
                $objAux = [
                    'label_period'   => $labelPeriod,
                    'period'         => $period,
                    'start_date'     => $date1,
                    'end_date'       => $date2,
                    'status'         => $status,
                    'payment_date'   => $paymentDate,
                    'payment_amount' => $paymentAmount,
                    'payment_plan'   => $paymentPlanName,
                    'bill_id'        => $bill_id,
                ];
                array_push($arrFeesAux, $objAux);
                $date1 = $date2;
                if ($date1 >= $today) {
                    $stop = true;
                }
                if($status=='pending')
                    $whileCount++;
                if($whileCount==6)
                    $stop = true;
                
                
            }
        }
     
        $arrFeesAux = array_reverse($arrFeesAux);
        return $arrFeesAux;
    }

    public function getFirstPendingFee($user)
    {
        $arrFeesAux = [];
        if ($user->rol == 'partner' && isset($user->partner)) {
            $createdAt = $user->partner->created_at;
            //$crearedAt = Carbon::parse($createdAt)->format('Y-m-d');
            $today = Carbon::today(); //->format('Y-m-d');

            $stop  = false;
            $date1 = $createdAt;
            $whileCount = 0;
            while ($stop == false) {
                $date2       = $date1->copy()->addMonth();
                $period      = $date1->format('d') . ' ' . $this->translateMonth($date1->format('m')) . ' ' . $date1->format('Y');
                $labelPeriod = $period;
                $paymentAux  = Payment::where('transaction_detail', $period)
                    ->where('user_id', $user->id)
                    ->where('partner_id', $user->partner->id)
                    ->where('payment_type', 'partner_fee')
                    ->orderBy('created_at', 'desc')
                    ->first();

                $status        = 'pending';
                $paymentDate   = '';
                $paymentAmount = '';
                
                // $bill_id = $paymentAux->bill_id;
                if ($paymentAux == null) {
                    $stop = true;
                    return $date1;
                }

                $date1 = $date2;
                if ($date1 >= $today) {
                    $stop = true;
                }
                
                if($status=='pending')
                    $whileCount++; 
                if($whileCount==6)
                    $stop = true;

            }
        }
      
    }

    public function getPartnerPaymentsStatus($user)
    {
        if (is_numeric($user))
        {
            $user = $this->findOrFail($user);
        }
        if($user->partner){
            if($user->partner->is_master=='1'){
                return ['status' => 'ok']; 
            }
        }
        $userFees = $this->generatePartnerMonthlyFees($user);
        $countAux = 0;
        foreach ($userFees as $fee) {
            if ($fee['status'] == 'pending' || $fee['status'] == 'processing') {
                $countAux++;
            }

        }
        if ($countAux == 0) {
            return ['status' => 'ok'];
        } else {
            return ['status' => 'error', 'pending_fees' => $countAux];
        }
    }

    public function payPartnerFee($data)
    {
        $data['payment_status'] = 'approved';
        //$data['bill_id']        = $this->factura->facturacion($data['payment_amount'], "Pago plan", $data['user_id']);
        $data['bill_id'] = 0;
        $payment = Payment::create($data);

        return $payment;
    }
    public function payAllPartnerFees($data)
    {
        $user = $this->findOrFail($data['user_id']);
        if ($user != null) {
            $userFees = $this->generatePartnerMonthlyFees($user);
            foreach ($userFees as $fee) {
                if ($fee['status'] == 'pending') {
                    $dataPayment = [
                        'transaction_detail' => $fee['period'],
                        //'transaction_id'          => 'required',
                        'payment_amount'     => $user->partner->plan->fee_value,
                        'payment_type'       => 'partner_fee',
                        'payment_method'     => 'manual',
                        'payment_status'     => 'approved',
                        'user_id'            => $user->id,
                        'plan_id'            => $user->partner->plan_id,
                        'partner_id'         => $user->partner->id,
                    ];
                    $this->payPartnerFee($dataPayment);
                }
            }
        }
        return $user;
    }


  

    public function getPartnerPendingFeesInfo($user)
    {
        if ($user != null) {
            $userFees = $this->generatePartnerMonthlyFees($user);
            $total    = 0;
            $periods  = [];
            $count    = 0;
            foreach ($userFees as $fee) {
                if ($fee['status'] == 'pending') {
                    $total += $user->partner->plan->fee_value;
                    array_push($periods, $fee['period']);
                    $count++;
                }
            }
            $arrPeriods   = [];
            $totalPeriods = count($periods);
            foreach ($periods as $key => $periodAux) {
                array_push($arrPeriods, $periodAux);
            }
            return ['total' => $total, 'periods' => $arrPeriods, 'pending_fees_amount' => $count];
        }
        return $user;

    }

 

}
