<?php 
namespace EnviosYa\Ordenes;

interface OrdenesRepositoryInterface
{
    public function findOrFail($id);
    public function create(array $data);
    public function update($entity, array $data);

    public function delete($entity);

    public function searchPartners($s);
    public function getorden($id);
    public function getcomercios();
    public function getUsers();
    public function generatePartnerMonthlyFees($entity);
    public function getPartnerPaymentsStatus($entity);


}