<?php 
namespace EnviosYa\Sections;

interface SectionsRepositoryInterface
{
    public function findOrFail($id);
    public function getsections();
    public function create(array $data);
    public function update($entity, array $data);
    public function delete($entity);



}