<?php

namespace Aatalac\User;

use Aatalac\Base\BaseEntity;

class Partner extends BaseEntity
{
    protected $fillable = ['user_id','plan_id','status','is_master']; 
    
    public function plan()
    {
        return $this->belongsTo('Aatalac\Plan\Plan');
    }
    public function user()
    {
        return $this->belongsTo('Aatalac\User\User');
    }
}	
