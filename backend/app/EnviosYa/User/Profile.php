<?php

namespace EnviosYa\User;

use EnviosYa\Base\BaseEntity;

class Profile extends BaseEntity
{
    protected $fillable = [ 'name', 'last_name', 'dni', 'birthdate', 'phone','phone_2','email_2', 'nationality', 'address', 'zip_code', 'country', 'state', 'city','cuil','observations','province_id', 'user_id','name_comercial'];

    // public function province()
    // {
    //     return $this->belongsTo('Aatalac\Province\Province');
    // }

}
