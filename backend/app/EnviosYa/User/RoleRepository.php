<?php

namespace EnviosYa\User;

use EnviosYa\Base\BaseRepository;
use EnviosYa\Payment\Payment;
use EnviosYa\User\Partner;
use EnviosYa\User\Profile;
use EnviosYa\User\User;
use Carbon\Carbon;
use File;
class RoleRepository extends BaseRepository implements RoleRepositoryInterface
{
    /**
     * @var User
     */
    protected $model;
    /**
     * UserRepository constructor.
     * @param User $model
     */
    public function __construct(User $model)
    {
        $this->model   = $model;
 
    }

    public function getModel()
    {
        return $this->model;
    }
    /**
     * @overwrite method create
     * @param  array $data
     * @return Aatalac\User\User $user
     */
    public function create(array $data)
    {
        try{
            $data['birthdate'] = Carbon::parse($data['birthdate'] )->format('Y-m-d');

        }catch(\Exception $e){
            $data['birthdate'] = null;
        }

        $data['password'] = bcrypt($data['password']);
        $user             = $this->model->create($data);
        $data['user_id']  = $user->id;
    
        Profile::create($data);
        if ($data['plan_id'] == 0) {
            $user->rol = 'user';
        } else {
            $user->rol   = 'partner';
            $dataPartner = ['user_id' => $user->id, 'plan_id' => $data['plan_id']];
            Partner::create($dataPartner);
        }
        $user->save();
        return $user;
    }
    /**
     * @overwrite method update
     * @param  array $data
     * @return Aatalac\User\User $user
     */
    public function update($user, array $data)
    {

        try{
            $data['birthdate'] = Carbon::parse($data['birthdate'] )->format('Y-m-d');

        }catch(\Exception $e){
            $data['birthdate'] = null;
        }
        
        if(isset($data['plan_id'])){
            if($user->partner != null ){
                $user->partner->plan_id = $data['plan_id'];
            }
        }
        if(isset($data['title'])){
            $path = public_path().'/titles/';
            if(!\File::exists($path)){
                \File::makeDirectory($path);
            }
            $file = \File($data['title']);
            var_dump($file);
            dd("a");
        }
        $user->fill($data);
        $user->profile->fill($data);
        $user->push();
        if($user->partner){
            $user->partner->status = $data['status'];
            $user->partner->is_master = $data['is_master'];
            $user->partner->save();
        }
        return $user;
    }

    public function updateWithTitle($user, array $data,$title)
    {

        try{
            $data['birthdate'] = Carbon::parse($data['birthdate'] )->format('Y-m-d');

        }catch(\Exception $e){
            $data['birthdate'] = null;
        }
        
        if(isset($data['plan_id'])){
            if($user->partner != null ){
                $user->partner->plan_id = $data['plan_id'];
            }
        }
        if($title){
            $path = public_path().'/images/titles/';
            if(!\File::exists($path)){
                \File::makeDirectory($path);
            }
            if(File::exists($path.$user->profile->title)){
                File::delete($path.$user->profile->title);
            }
           
            $fileName = $user->id.'_title.'.$title->getClientOriginalExtension();
            $user->profile->title = $fileName;
            $title->move($path, $fileName);
            $user->profile->save();
           
        }
        $user->fill($data);
        $user->profile->fill($data);
        $user->push();
        if($user->partner){
            $user->partner->status = $data['status'];
            $user->partner->is_master = $data['is_master'];
            $user->partner->save();
        }
        return $user;
    }
     
    public function updatepago_recur($user, array $data)
    {
     

    $user->automatic_debit = $data['automatic_debit'];
    $user->id_recurente = $data['id_recurente'];
 
   
    $user->save();
      return $user;

    }


    /**
     * @overwrite method delete
     * @param  Aatalac\User\User $user
     * @return boolean
     */
    public function delete($user)
    {
        if (is_numeric($user)) {
            $user = $this->findOrFail($user);
        }
        if (count($user->commissions) <= 0) {
            $this->deleteImage($user->getPath(), $user->image);
            $user->delete();
            return true;
        }

        return false;
    }

    /**
     *
     * @param  array $data
     * @return Aatalac\User\User $user
     */
    public function findOrCreate(array $data)
    {
        $userAux         = $this->getModel()::where('email', $data['email'])->first();
        $data['plan_id'] = 0;
        if ($userAux == null) {
            $userAux         = $this->create($data);
            $data['user_id'] = $userAux->id;
        }

        return $userAux;
    }

    /**
     * Get sorted courses by position
     *
     * @return [] Aatalac\User\User $users
     */
    public function getSortedCourses()
    {
        return $this->getModel()->orderBy("position", "asc")->get();
    }
    public function getPartners(){
        return $this->getModel()->orderBy("created_at", "asc")
                                ->where('rol', '=', 'partner')
                                ->where('status','active')
                                ->get();
    }
    public function searchPartners($s = '')
    {
        
        return  $this->getModel()
                    ->join('profiles', 'users.id', '=', 'profiles.user_id')
                    ->join('partners', 'users.id', '=', 'partners.user_id')
                    ->join('plans', 'partners.plan_id', '=', 'plans.id')
                    ->where('users.rol', '=', 'partner')
                    ->where('users.email','like','%'.$s.'%')
                    ->orWhere('profiles.name','like','%'.$s.'%')
                    ->where('users.rol', '=', 'partner')
                    ->orWhere('profiles.last_name','like','%'.$s.'%')
                    ->where('users.rol', '=', 'partner')
                    ->orderBy("partners.id", "asc")
                    ->select('users.id','users.email','profiles.name','profiles.last_name','partners.id as partner_id','plans.name as plan_name')
                    ->paginate(50);

    }

    public function getUsers($s = '')
    {
        // return $this->getModel()->orderBy("created_at", "asc")->where('rol', '=', 'partner')->paginate(50);
        return  $this->getModel()
                    ->join('profiles', 'users.id', '=', 'profiles.user_id')
                    ->whereIn('users.rol',['user','pending_partner'])
                    ->where('users.email','like','%'.$s.'%')
                    ->orWhere('profiles.name','like','%'.$s.'%')
                    ->whereIn('users.rol',['user','pending_partner'])
                    ->orWhere('profiles.last_name','like','%'.$s.'%')
                    ->whereIn('users.rol',['user','pending_partner'])
                    ->orderBy("users.created_at", "asc")
                    ->orderBy("users.created_at", "asc")
                    ->select('users.id','users.email','profiles.name','profiles.last_name')
                    ->paginate(50);
        
    }
    public function translateMonth($num)
    {
        $num        = $num - 1;
        $arrSpanish = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];
        return $arrSpanish[$num];
    }
    public function generatePartnerMonthlyFees($user)
    {
        $arrFeesAux = [];
        if ($user->rol == 'partner' && isset($user->partner)) {
            /*Si es socio maestro no retorna vacio ya que no tiene que pagar cuotas*/
            if($user->partner->is_master=='1')
                return [];
            $createdAt = $user->partner->created_at;
            //$crearedAt = Carbon::parse($createdAt)->format('Y-m-d');
            $today = Carbon::today(); //->format('Y-m-d');

            $stop  = false;
            $date1 = $createdAt;
            $whileCount = 0;
            while ($stop == false) {
                $date2       = $date1->copy()->addMonth();
                $period      = $date1->format('d') . ' ' . $this->translateMonth($date1->format('m')) . ' ' . $date1->format('Y');
                $labelPeriod = $period;
                $paymentAux  = Payment::where('transaction_detail', $period)
                    ->where('user_id', $user->id)
                    ->where('partner_id', $user->partner->id)
                    ->where('payment_type', 'partner_fee')
                    ->orderBy('created_at', 'desc')
                    ->first();

                $status        = 'pending';
                $paymentDate   = '';
                $paymentAmount = '';
                // $bill_id = $paymentAux->bill_id;
                if ($paymentAux != null) {
                    if ($paymentAux->payment_status == 'approved') {
                        $status = 'paid';
                    }
                    $paymentDate     = Carbon::parse($paymentAux->created_at)->format('Y-m-d');
                    $paymentAmount   = $paymentAux->payment_amount;
                    $paymentPlanName = $paymentAux->plan->name;
                    $bill_id         = $paymentAux->bill_id;
                } else {
                    $paymentPlanName = "";
                    $bill_id         = "";
                    
                }

                //print_r($paymentAux->bill_id);
                $objAux = [
                    'label_period'   => $labelPeriod,
                    'period'         => $period,
                    'start_date'     => $date1,
                    'end_date'       => $date2,
                    'status'         => $status,
                    'payment_date'   => $paymentDate,
                    'payment_amount' => $paymentAmount,
                    'payment_plan'   => $paymentPlanName,
                    'bill_id'        => $bill_id,
                ];
                array_push($arrFeesAux, $objAux);
                $date1 = $date2;
                if ($date1 >= $today) {
                    $stop = true;
                }
                if($status=='pending')
                    $whileCount++;
                if($whileCount==6)
                    $stop = true;
                
                
            }
        }
     
        $arrFeesAux = array_reverse($arrFeesAux);
        return $arrFeesAux;
    }

    public function getFirstPendingFee($user)
    {
        $arrFeesAux = [];
        if ($user->rol == 'partner' && isset($user->partner)) {
            $createdAt = $user->partner->created_at;
            //$crearedAt = Carbon::parse($createdAt)->format('Y-m-d');
            $today = Carbon::today(); //->format('Y-m-d');

            $stop  = false;
            $date1 = $createdAt;
            $whileCount = 0;
            while ($stop == false) {
                $date2       = $date1->copy()->addMonth();
                $period      = $date1->format('d') . ' ' . $this->translateMonth($date1->format('m')) . ' ' . $date1->format('Y');
                $labelPeriod = $period;
                $paymentAux  = Payment::where('transaction_detail', $period)
                    ->where('user_id', $user->id)
                    ->where('partner_id', $user->partner->id)
                    ->where('payment_type', 'partner_fee')
                    ->orderBy('created_at', 'desc')
                    ->first();

                $status        = 'pending';
                $paymentDate   = '';
                $paymentAmount = '';
                
                // $bill_id = $paymentAux->bill_id;
                if ($paymentAux == null) {
                    $stop = true;
                    return $date1;
                }

                $date1 = $date2;
                if ($date1 >= $today) {
                    $stop = true;
                }
                
                if($status=='pending')
                    $whileCount++; 
                if($whileCount==6)
                    $stop = true;

            }
        }
      
    }

    public function getPartnerPaymentsStatus($user)
    {
        if (is_numeric($user))
        {
            $user = $this->findOrFail($user);
        }
        if($user->partner){
            if($user->partner->is_master=='1'){
                return ['status' => 'ok']; 
            }
        }
        $userFees = $this->generatePartnerMonthlyFees($user);
        $countAux = 0;
        foreach ($userFees as $fee) {
            if ($fee['status'] == 'pending' || $fee['status'] == 'processing') {
                $countAux++;
            }

        }
        if ($countAux == 0) {
            return ['status' => 'ok'];
        } else {
            return ['status' => 'error', 'pending_fees' => $countAux];
        }
    }

    public function payPartnerFee($data)
    {
        $data['payment_status'] = 'approved';
        //$data['bill_id']        = $this->factura->facturacion($data['payment_amount'], "Pago plan", $data['user_id']);
        $data['bill_id'] = 0;
        $payment = Payment::create($data);

        return $payment;
    }
    public function payAllPartnerFees($data)
    {
        $user = $this->findOrFail($data['user_id']);
        if ($user != null) {
            $userFees = $this->generatePartnerMonthlyFees($user);
            foreach ($userFees as $fee) {
                if ($fee['status'] == 'pending') {
                    $dataPayment = [
                        'transaction_detail' => $fee['period'],
                        //'transaction_id'          => 'required',
                        'payment_amount'     => $user->partner->plan->fee_value,
                        'payment_type'       => 'partner_fee',
                        'payment_method'     => 'manual',
                        'payment_status'     => 'approved',
                        'user_id'            => $user->id,
                        'plan_id'            => $user->partner->plan_id,
                        'partner_id'         => $user->partner->id,
                    ];
                    $this->payPartnerFee($dataPayment);
                }
            }
        }
        return $user;
    }

    public function getUserCourseRateType($user, $location_id)
    {
        $userStatus = $this->getPartnerPaymentsStatus($user);
        $userRol    = $user->rol;
        if ($userStatus['status'] == 'ok') {
            if ($userRol == 'partner' && $location_id != 5) {
                return 'socio_presencial';
            }
            if ($userRol == 'partner' && $location_id == 5) {
                return 'socio_distancia';
            }
        } else {
            $userRol = 'user';
        }
        if ($userRol == 'user' && $location_id != 5) {
            return 'no_socio_presencial';
        }
        if ($userRol == 'user' && $location_id == 5) {
            return 'no_socio_distancia';
        }
    }
    public function getUserCourseRate($user, $commission)
    {
        $rateIdAux = $user->commissions()->find($commission->id)->pivot->rate_id;
        //$rate = \Aatalac\Rate\Rate::find($rateIdAux);
        $rate = $commission->rates()->find($rateIdAux);
        //$userType = $this->getUserCourseRateType($user, $commission->pivot->location_id);
        //$rate     = $commission->rates()->where('type', '=', $userType)->first();
        return $rate;
    }
    public function getUserRate($user, $commission, $location_id)
    {
        $userType = $this->getUserCourseRateType($user, $location_id);
        $rate     = $commission->rates()->where('type', '=', $userType)->first();
        return $rate;
    }
    public function generateUserCourseMonthlyFees($user, $rate)
    {

        $fees = $rate->monthly_fees()->orderBy('expiration_date', 'asc')->get();

        foreach ($fees as $key => $fee) {

            $transactionDetail = 'Cuota' . ($key + 1);
            $paymentAux        = Payment::where('user_id', '=', $user->id)
            //->where('transaction_detail','=',$transactionDetail)
                ->where('payment_type', 'course_fee')
                ->where('monthly_fee_id', $fee->id)
                ->where('payment_status', 'approved')
                ->orderBy('created_at', 'desc')
                ->first();

            $status        = 'pending';
            $paymentDate   = '';
            $paymentAmount = '';
            $billId = 0;
            if ($paymentAux != null) {
                $status        = 'paid';
                $paymentDate   = Carbon::parse($paymentAux->created_at)->format('Y-m-d');
                $paymentAmount = $paymentAux->payment_amount;
                $billId = $paymentAux->bill_id;
            } else {
                $paymentPlanName = "";
            }
            $fee['label']          = 'Cuota' . ($key + 1);
            $fee['status']         = $status;
            $fee['payment_date']   = $paymentDate;
            $fee['payment_amount'] = $paymentAmount;
            $fee['bill_id']        = $billId;

        }
        return $fees->reverse();
    }
    public function getCoursePaymentsStatus($user, $rate)
    {
       
        $userFees = $this->generateUserCourseMonthlyFees($user, $rate);
        $countAux = 0;
        foreach ($userFees as $fee) {
            if ($fee['status'] == 'pending') {
                $countAux++;
            }

        }
        if ($countAux == 0) {
            return ['status' => 'ok'];
        } else {
            return ['status' => 'error', 'pending_fees' => $countAux];
        }
    }
    public function getCommissionInscription($user, $rate)
    {
        $paymentAux = Payment::where('user_id', '=', $user->id)
        //->where('transaction_detail','=',$transactionDetail)
            ->where('payment_type', '=', 'course_inscription')
            ->where('commission_id', '=', $rate->commission_id)
            ->where('rate_id', '=', $rate->id)
            ->orderBy('created_at', 'desc')
            ->first();

        $inscription['label'] = "Inscripción";
        if ($paymentAux == null) {
            $inscription['status']         = "pending";
            $inscription['payment_date']   = "";
            $inscription['payment_amount'] = $rate->inscription_value;

        } else {
            $inscription['status']         = "paid";
            $inscription['payment_date']   = Carbon::parse($paymentAux->created_at)->format('Y-m-d');
            $inscription['payment_amount'] = $paymentAux->payment_amount;
            $inscription['bill_id'] = $paymentAux->bill_id;
        }
        return $inscription;
    }

    public function getPartnerPendingFeesInfo($user)
    {
        if ($user != null) {
            $userFees = $this->generatePartnerMonthlyFees($user);
            $total    = 0;
            $periods  = [];
            $count    = 0;
            foreach ($userFees as $fee) {
                if ($fee['status'] == 'pending') {
                    $total += $user->partner->plan->fee_value;
                    array_push($periods, $fee['period']);
                    $count++;
                }
            }
            $arrPeriods   = [];
            $totalPeriods = count($periods);
            foreach ($periods as $key => $periodAux) {
                array_push($arrPeriods, $periodAux);
            }
            return ['total' => $total, 'periods' => $arrPeriods, 'pending_fees_amount' => $count];
        }
        return $user;

    }
    public function validateUserRate($user, $rate)
    {
        $userAux = $this->getModel()::find($user->id);
        if ($rate->student_type->id == 1) {
            $newRate = $rate->commission->rates()->where('type', 'no_socio_presencial')->first();
            if($newRate==null){
                $newRate = $rate;
            }
        } else if ($rate->student_type->id == 2) {
            $newRate = $rate->commission->rates()->where('type', 'no_socio_distancia')->first();
            if($newRate==null){
                $newRate = $rate;
            }
        } else {
            $newRate = $rate;
        }
        if ($userAux->rol == 'partner') {
            /*Verificacion de socio_presencial(1) y socio_distancia(2) */
            if ($rate->student_type->id == 1 || $rate->student_type->id == 2) {

                $userStatus = $this->getPartnerPaymentsStatus($userAux);
                if ($userStatus['status'] == 'ok') {
                    return ['status' => true, 'message' => 'El usuario es socio y esta al día.'];
                } else {
                    return ['status' => false, 'message' => ' Estimado alumno su email está registrado como socio pero tiene cuotas pendientes. Si regulariza la situación podrá inscribirse como socio y obtener el beneficio del descuento exclusivo.', 'new_rate' => $newRate];
                }
            }
        } else {
            return [
                'status'   => false,
                'message'  => 'El usuario no es un socio',
                'new_rate' => $newRate,
            ];
        }
        return ['status' => true];
    }
    public function validateUserRatePartners($user, $rate)
    {

        $userAux = $this->getModel()::find($user->id);
        if ($rate->student_type->id == 3) {
            $newRate = $rate->commission->rates()->where('type', 'socio_presencial')->first();
            if($newRate==null){
                $newRate = $rate;
            }
        } else if ($rate->student_type->id == 4) {
            $newRate = $rate->commission->rates()->where('type', 'socio_distancia')->first();
            if($newRate==null){
                $newRate = $rate;
            }
        } else {
            $newRate = $rate;
        }
        if ($userAux->rol == 'partner') {
            /*Verificacion de socio_presencial(1) y socio_distancia(2) */
            if ($rate->student_type->id == 3 || $rate->student_type->id == 4) {
                $userStatus = $this->getPartnerPaymentsStatus($userAux);
                if ($userStatus['status'] == 'ok') {
                    return ['status' => false, 'message' => 'El usuario es socio y esta al día.', 'new_rate' => $newRate];
                }
            }
        }
        return ['status' => true];
    }

    public function getPartnersByDate($date){
        $partners =  Partner::where('created_at','like','%'.$date.'%')
                            ->where('status','active')
                            ->where('is_master','0')
                            ->get();
        return $partners;
    }

    public function getPartnersWithDebt(){
        $usersAux = \Aatalac\User\Partner::where('is_master','0')->get();
        $totalDebt = 0;
        foreach($usersAux as $key => $partnerAux){
            $status = $this->getPartnerPaymentsStatus($partnerAux->user);
            $now = Carbon::now();
            if($status['status']=='error'){
                $diffInMonths = $now->diffInMonths($this->getFirstPendingFee($partnerAux->user));

                if($diffInMonths <= 5){
                    $totalAux = $status['pending_fees'] * $partnerAux->plan->fee_value;
                    $partnerAux->total_debt = $totalAux ;
                    $totalDebt += $totalAux;
                }else{
                     unset($usersAux[$key]);
                }
                
                
            }else{
                unset($usersAux[$key]);
            }
            
            
            
        }
        return['total_debt'=> $totalDebt, 'users'=>$usersAux] ;
    }
    public function getUsersWithCourseDebt(){
        $commissions = \Aatalac\Commission\Commission::all();
        $totalDebt = 0;
        $retUsers = [];
        foreach($commissions as $commission){
            if(count($commission->users)>0){
                $usersAux = $commission->users;
                foreach( $usersAux as $key => $courseUser){
                    $rateAux = $this->getUserCourseRate($courseUser,$commission);
                    $status = $this-> getCoursePaymentsStatus($courseUser, $rateAux);
                    if($status['status']=='error'){
                        $totalAux = $status['pending_fees'] * $rateAux->fee_value;
                        $courseUser->total_debt = $totalAux ;
                        $courseUser->concept = $commission->course->name.' '.$commission->name;
                        $totalDebt += $totalAux;
                        array_push($retUsers, $courseUser);
                    }
                    //echo $courseUser->email.' '.json_encode($status);
                }
            }
        }
        return['total_debt'=> $totalDebt, 'course_users'=>$retUsers] ;
    }

    public function createPartnerToUser($user, $plan_id){
        $user->rol = 'partner';
        $user->save();
        $dataPartner = ['user_id' => $user->id, 'plan_id' => $plan_id];
        Partner::create($dataPartner);
        return $user;
    }
}
