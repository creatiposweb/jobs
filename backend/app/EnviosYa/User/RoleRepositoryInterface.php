<?php 
namespace EnviosYa\User;

interface RoleRepositoryInterface
{
    public function findOrFail($id);
    public function create(array $data);
    public function update($entity, array $data);
    public function updatepago_recur($entity, array $data);
    public function delete($entity);
    public function getPartners();
    public function searchPartners($s);
    public function getUsers();
    public function generatePartnerMonthlyFees($entity);
    public function getPartnerPaymentsStatus($entity);
    public function getUserCourseRateType($entity,$location_id);
    public function getUserCourseRate($entity,$commission);
    public function generateUserCourseMonthlyFees($entity,$rate);
}