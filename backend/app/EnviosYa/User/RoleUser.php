<?php

namespace EnviosYa\User;

use EnviosYa\Base\BaseEntity;
use App\User;
use Illuminate\Database\Eloquent\Model;

class RoleUser extends Model
{
       
protected $table = 'role_user';

protected $fillable = [ 'user_id', 'role_id'];	


    public $timestamps = false;
    protected $guarded = ['id'];



}
