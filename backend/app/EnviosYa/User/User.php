<?php

namespace EnviosYa\User;

use EnviosYa\Base\BaseEntity;

class User extends BaseEntity
{
    protected $fillable = ['username','email','password','name'];
   	protected $hidden = ['password'];

   	public function profile()
    {
        return $this->hasOne('EnviosYa\User\Profile');
    }

    public function partner()
    {
        return $this->hasOne('EnviosYa\User\Partner');
    }

    public function payments()
    {
        return $this->hasMany('EnviosYa\Payment\Payment');
    }

        public function rol()
    {
        return $this->hasOne('EnviosYa\User\Profile');
    }


}	
