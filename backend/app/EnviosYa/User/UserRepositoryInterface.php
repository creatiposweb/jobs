<?php 
namespace EnviosYa\User;

interface UserRepositoryInterface
{
    public function findOrFail($id);
    public function create(array $data);
    public function update($entity, array $data);

    public function delete($entity);

    public function searchPartners($s);
    public function getcadeterias();
    public function getcomercios();
    public function getcomensales();
    public function getUsers();
    public function generatePartnerMonthlyFees($entity);
    public function getPartnerPaymentsStatus($entity);


}