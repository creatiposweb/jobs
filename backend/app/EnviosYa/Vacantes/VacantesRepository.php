<?php

namespace EnviosYa\Vacantes;

use EnviosYa\Base\BaseRepository;
use EnviosYa\Payment\Payment;
use EnviosYa\User\Partner;
use EnviosYa\User\Profile;
use EnviosYa\User\RoleUser;
use EnviosYa\User\User;
use EnviosYa\Vacantes\Vacantes;
use Carbon\Carbon;
use File;
use Auth;

class VacantesRepository extends BaseRepository implements VacantesRepositoryInterface
{
    /**
     * @var vacantes
     */
    protected $model;
    /**
     * UserRepository constructor.
     * @param Vacantes $model
     */
    public function __construct(Vacantes $model)
    {
        $this->model   = $model;
 
    }

    public function getModel()
    {
        return $this->model;
    }

    /**
     * @overwrite method create
     * @param  array $data
     * @return Aatalac\User\User $user
     */
    public function create(array $data)
    {


        $data['users_id'] = Auth::user()->id;
        $data['sections_id'] = $data['seccion'];
        $data['slug']  = str_slug($data['title']); //salug url SEO AMIGABLE

        $news            = $this->model->create($data);

        return $news;
        
    }
    /**
     * @overwrite method update
     * @param  array $data
     * @return Aatalac\User\User $user
     */
    public function update($user, array $data)
    {

  
        
        // if(isset($data['plan_id'])){
        //     if($user->partner != null ){
        //         $user->partner->plan_id = $data['plan_id'];
        //     }
        // }
        // if(isset($data['title'])){
        //     $path = public_path().'/titles/';
        //     if(!\File::exists($path)){
        //         \File::makeDirectory($path);
        //     }
        //     $file = \File($data['title']);
        //     var_dump($file);
        //     dd("a");
        // }
        $user->fill($data);
        //$user->profile->fill($data);
        $user->push();
        // if($user->partner){
        //     $user->partner->status = $data['status'];
        //     $user->partner->is_master = $data['is_master'];
        //     $user->partner->save();
        // }
        return $user;
    }


     



    /**
     * @overwrite method delete
     * @param  Aatalac\User\User $user
     * @return boolean
     */
    public function delete($user)
    {
        if (is_numeric($user)) {
            $user = $this->findOrFail($user);
        }
        if (count($user->commissions) <= 0) {
            $this->deleteImage($user->getPath(), $user->image);
            $user->delete();
            return true;
        }

        return false;
    }

    public function translateMonth($num)
    {
        $num        = $num - 1;
        $arrSpanish = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];
        return $arrSpanish[$num];
    }

   





}
