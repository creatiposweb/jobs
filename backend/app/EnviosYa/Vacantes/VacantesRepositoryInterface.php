<?php 
namespace EnviosYa\Vacantes;

interface VacantesRepositoryInterface
{
    public function findOrFail($id);
    public function create(array $data);
    public function update($entity, array $data);
    public function delete($entity);



}