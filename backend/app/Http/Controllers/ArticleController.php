<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use EnviosYa\User\Role;
use EnviosYa\User\UserRepositoryInterface;
use EnviosYa\Articles\ArticlesRepositoryInterface;

class ArticleController extends Controller
{

private $articles;



        public function __construct(ArticlesRepositoryInterface $articles)
    {
       // $this->middleware(['auth']);
        $this->articles = $articles;
      
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
              
              $data['items'] = $this->articles->getsections();
              $data['code'] = 20000;

              
               return response()->json($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

         $data['items'] = $this->articles->create($request->all());

         $data['code'] = 20000;

              
         return response()->json($data);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        
        $usuario = $this->articles->findOrFail($request->id);
        
        $this->articles->update($usuario, $request->all());

        $data['code'] = 20000;

              
         return response()->json($data);


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
