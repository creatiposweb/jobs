<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use EnviosYa\User\Role;
use EnviosYa\User\UserRepositoryInterface;

class ComerciosController extends Controller
{

private $cadeterias;



        public function __construct(UserRepositoryInterface $cadeterias)
    {
       // $this->middleware(['auth']);
        $this->cadeterias = $cadeterias;
      
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
              
              $data['items'] = $this->cadeterias->getcomercios();
              $data['code'] = 20000;

              
               return response()->json($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

         $data['items'] = $this->cadeterias->create($request->all());

         $data['code'] = 20000;

              
         return response()->json($data);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        
        $usuario = $this->cadeterias->findOrFail($request->id);
        
        $this->cadeterias->update($usuario, $request->all());

                 $data['code'] = 20000;

              
         return response()->json($data);


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
