<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use EnviosYa\User\Role;
use EnviosYa\Ordenes\OrdenesRepositoryInterface;
use Auth;
class OrdenesController extends Controller
{

private $ordenes;



        public function __construct(OrdenesRepositoryInterface $ordenes)
    {
       // $this->middleware(['auth']);
        $this->ordenes = $ordenes;
      
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id = Auth::user()->id;
              $data['items'] = $this->ordenes->getorden($id);
              $data['code'] = 20000;

              
               return response()->json($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

         $data['items'] = $this->ordenes->create($request->all());

         $data['code'] = 20000;

              
         return response()->json($data);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        
        $usuario = $this->ordenes->findOrFail($request->id);
        
        $this->ordenes->update($usuario, $request->all());

                 $data['code'] = 20000;

              
         return response()->json($data);


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
