<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\pacientes;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use App\User;
use EnviosYa\User\Role;
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


        public function authenticate(Request $request)
    {
        $credentials = $request->only('username', 'password');

       // print_r($credentials);
       

        if (Auth::attempt($credentials)) {
            // Authentication passed...
            //return redirect()->intended('dashboard');
            return response()->json(['code' => 20000, 'data' => ['token'=>'admin-token']]);
        }
         return response()->json(['code' => 50008, 'data' => ['token'=>'admin-token']]);
    }



    public function index()
    {
        $user = Auth::user()->roles;

        print_r($user->pivotParent->name);

        //return pacientes::get();


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
  

        public function show()
    {

         $user = Auth::user();
 
        
        return response()->json(['code' => 20000, 'data' => ['roles'=>['admin'],'introduction'=>'I am a super administrator','avatar'=>"https://creatipos.com.ar/favicon.png",'name'=>$user->name]]);


    }

    

            public function salir()
    {
        
            return response()->json(['code' => 20000, 'data' => "success"]);


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
