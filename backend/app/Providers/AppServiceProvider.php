<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
         Schema::defaultStringLength(191);

          $this->app->bind('EnviosYa\\User\\UserRepositoryInterface', 'EnviosYa\\User\\UserRepository');
          $this->app->bind('EnviosYa\\Ordenes\\OrdenesRepositoryInterface', 'EnviosYa\\Ordenes\\OrdenesRepository');
          $this->app->bind('EnviosYa\\Menu\\MenuRepositoryInterface', 'EnviosYa\\Menu\\MenuRepository');
          $this->app->bind('EnviosYa\\Sections\\SectionsRepositoryInterface', 'EnviosYa\\Sections\\SectionsRepository');
          $this->app->bind('EnviosYa\\Articles\\ArticlesRepositoryInterface', 'EnviosYa\\Articles\\ArticlesRepository');
          $this->app->bind('EnviosYa\\Vacantes\\VacantesRepositoryInterface', 'EnviosYa\\Vacantes\\VacantesRepository');
          $this->app->bind('EnviosYa\\Area\\AreaRepositoryInterface', 'EnviosYa\\Area\\AreaRepository');
          
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
