<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticlesTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'articles';

    /**
     * Run the migrations.
     * @table articles
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->longText('header_label')->nullable();
            $table->longText('title')->nullable();
            $table->longText('lead')->nullable();
            $table->longText('body')->nullable();
            $table->longText('slug')->nullable();
            $table->unsignedInteger('sections_id');
            $table->unsignedInteger('users_id');

            $table->index(["sections_id"], 'fk_articles_sections1_idx');

            $table->index(["users_id"], 'fk_articles_users1_idx');
            $table->nullableTimestamps();


            $table->foreign('sections_id', 'fk_articles_sections1_idx')
                ->references('id')->on('sections')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('users_id', 'fk_articles_users1_idx')
                ->references('id')->on('users')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
