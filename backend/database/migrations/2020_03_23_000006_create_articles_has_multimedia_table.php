<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticlesHasMultimediaTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'articles_has_multimedia';

    /**
     * Run the migrations.
     * @table articles_has_multimedia
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->unsignedInteger('articles_id');
            $table->unsignedInteger('multimedia_id');

            $table->index(["articles_id"], 'fk_articles_has_multimedia_articles1_idx');

            $table->index(["multimedia_id"], 'fk_articles_has_multimedia_multimedia1_idx');


            $table->foreign('articles_id', 'fk_articles_has_multimedia_articles1_idx')
                ->references('id')->on('articles')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('multimedia_id', 'fk_articles_has_multimedia_multimedia1_idx')
                ->references('id')->on('multimedia')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
