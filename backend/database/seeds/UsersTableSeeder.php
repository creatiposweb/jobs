<?php

use Illuminate\Database\Seeder;
use EnviosYa\User\Role;
use EnviosYa\User\User;

class UsersTableSeeder extends Seeder
{
   /**
   * Run the database seeds.
   *
   * @return void
   */
   public function run()
   {


      // rol administrador
      $admin= new Role;
      $admin->name= "Administrador";
      $admin->slug= "admin";
      $admin->description= "Administrador del sistema";
      $admin->level= "1";
      $admin->save();

      
      // user
      $user= new User;
      $user->name= "ADMINISTRADOR";
      $user->email= "creatiposweb@gmail.com";
      $user->username= "creatipos";
      $user->password= bcrypt('123456');
      $user->roles_id= $admin->id;
      $user->save();
   }

}