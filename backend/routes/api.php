<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


//Agregamos nuestra ruta al controller de Pokemons
Route::resource('pacientes', 'PacientesController');

//Route::resource('cadeterias', 'CadeteriasController');

Route::post('user/login', 'UserController@authenticate');
Route::post('login', 'UserController@authenticate');

Route::get('user/info', 'UserController@show');

Route::get('user', 'UserController@index');

Route::post('user/logout', 'UserController@salir');

Route::group(['prefix' => 'cadeterias'], function () {

    Route::get("/", "CadeteriasController@index");
    Route::post("/", "CadeteriasController@store");
    Route::put("/", "CadeteriasController@update");

});

Route::group(['prefix' => 'comensales'], function () {

    Route::get("/", "ComensalesController@index");
    Route::post("/", "ComensalesController@store");
    Route::put("/", "ComensalesController@update");

});

Route::group(['prefix' => 'menu'], function () {

    Route::get("/", "MenuController@index");
    Route::post("/", "MenuController@store");
    Route::put("/", "MenuController@update");

});


Route::group(['prefix' => 'comercios'], function () {

    Route::get("/", "ComerciosController@index");
    Route::post("/", "ComerciosController@store");
    Route::put("/", "ComerciosController@update");

});

Route::group(['prefix' => 'ordenes'], function () {

    Route::get("/", "OrdenesController@index");
    Route::post("/", "OrdenesController@store");
    Route::put("/", "OrdenesController@update");

});

Route::group(['prefix' => 'sections'], function () {

    Route::get("/list", "SectionsController@index");
    Route::post("/", "SectionsController@store");
    Route::put("/", "SectionsController@update");

});


Route::group(['prefix' => 'article'], function () {

    Route::get("/list", "ArticleController@index");
    Route::post("/create", "ArticleController@store");
    Route::put("/update", "ArticleController@update");

});

Route::group(['prefix' => 'vacantes'], function () {

    Route::get("/list", "VacantesController@index");
    Route::post("/create", "VacantesController@store");
    Route::put("/update", "VacantesController@update");

});

Route::group(['prefix' => 'base'], function () {

    Route::get("/area", "BaseController@get_area");
    Route::get("/subarea/{id}", "BaseController@get_subarea");
  


});




