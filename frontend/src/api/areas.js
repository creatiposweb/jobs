import request from '@/utils/request'

export function fetchAreas(query) {
  return request({
    url: '/areas/list',
    method: 'get',
    params: query
  })
}



export function fetchSubarea(query) {
    return request({
      url: '/areas/sub',
      method: 'get',
      params: query
    })
  }

