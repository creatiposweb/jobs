import request from '@/utils/request'

export function fetchAreas(query) {
  return request({
    url: '/base/area',
    method: 'get',
    params: query
  })
}


export function fetchSubarea(id) {
    return request({
      url: '/base/subarea/'+id,
      method: 'get',
      params: id
    })
  }

  export function fetchUbicaciones(query) {
    return request({
      url: '/base/subarea',
      method: 'get',
      params: query
    })
  }
