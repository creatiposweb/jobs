import request from '@/utils/request'

export function fetchList(query) {
  return request({
    url: '/cadeterias',
    method: 'get',
    params: query
  })
}

export function fetchArticle(id) {
  return request({
    url: '/article/detail',
    method: 'get',
    params: { id }
  })
}

export function fetchPv(pv) {
  return request({
    url: '/article/pv',
    method: 'get',
    params: { pv }
  })
}

export function createArticle(data) {
  return request({
    url: '/cadeterias',
    method: 'post',
    data
  })
}

export function updateArticle(data) {
  return request({
    url: '/cadeterias',
    method: 'put',
    data
  })
}
