import request from '@/utils/request'

export function sectionsList(query) {
  return request({
    url: '/sections/list',
    method: 'get',
    params: query
  })
}

export function fetchSection(id) {
  return request({
    url: '/sections/detail',
    method: 'get',
    params: { id }
  })
}

export function fetchPv(pv) {
  return request({
    url: '/sections/pv',
    method: 'get',
    params: { pv }
  })
}

export function createSection(data) {
  return request({
    url: '/sections/create',
    method: 'post',
    data
  })
}

export function updateSection(data) {
  return request({
    url: '/sections/update',
    method: 'post',
    data
  })
}
