import request from '@/utils/request'

export function fetchUbicaciones(query) {
  return request({
    url: '/ubicaciones/list',
    method: 'get',
    params: query
  })
}

