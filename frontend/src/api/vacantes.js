import request from '@/utils/request'

export function fetchList(query) {
  return request({
    url: '/vacantes/list',
    method: 'get',
    params: query
  })
}

export function fetchVacante(id) {
  return request({
    url: '/vacantes/detail',
    method: 'get',
    params: { id }
  })
}

export function fetchPv(pv) {
  return request({
    url: '/vacantes/pv',
    method: 'get',
    params: { pv }
  })
}

export function createVacante(data) {
  return request({
    url: '/vacantes/create',
    method: 'post',
    data
  })
}

export function updateVacante(data) {
  return request({
    url: '/vacantes/update',
    method: 'post',
    data
  })
}
