import Vue from 'vue'

import Cookies from 'js-cookie'

import 'normalize.css/normalize.css' // a modern alternative to CSS resets

import Element from 'element-ui'
import './styles/element-variables.scss'

import '@/styles/index.scss' // global css

import App from './App'
import store from './store'
import router from './router'
import locale from 'element-ui/lib/locale/lang/es'
import './icons' // icon
import './permission' // permission control
import './utils/error-log' // error log
import moment from 'moment'
import * as filters from './filters' // global filters
import  MediaLibrary  from  'vue-medialibrary';
import * as VueGoogleMaps from "vue2-google-maps";
/**
 * If you don't want to use mock-server
 * you want to use MockJs for mock api
 * you can execute: mockXHR()
 *
 * Currently MockJs will be used in the production environment,
 * please remove it before going online! ! !
 */
import Vuetify from 'vuetify'
import DaySpanVuetify from 'dayspan-vuetify';

import 'vuetify/dist/vuetify.min.css'
import 'material-design-icons-iconfont/dist/material-design-icons.css'
import 'dayspan-vuetify/dist/lib/dayspan-vuetify.min.css'

import { mockXHR } from '../mock'
if (process.env.NODE_ENV === 'production') {
    //mockXHR()
}

Vue.use(Vuetify);

Vue.filter('formatDate', function(value) {
    if (value) {
        return moment(String(value)).format('MM/DD/YYYY hh:mm')
    }
})

Vue.use(MediaLibrary, {
    API_URL: 'http://localhost:8000/'
});


Vue.use(DaySpanVuetify, {
    methods: {
     // getDefaultEventColor: () => '#FFA07A'
    }
  });

Vue.use(Element, { locale })

Vue.use(Element, {
    size: Cookies.get('size') || 'medium' // set element-ui default size
})

Vue.use(VueGoogleMaps, {
    load: {
        key: "AIzaSyDAWVmG0qvOzm20EGJXq2GAY1IjTwXL34M",
        libraries: "places" // necessary for places input
    }
});

// register global utility filters
Object.keys(filters).forEach(key => {
    Vue.filter(key, filters[key])
})

Vue.config.productionTip = false

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
