/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/layout'

const pacientesRouter = {
    path: '/cadeterias',
    component: Layout,
    redirect: '/abm-cadeterias/complex-table',
    name: 'Cadeterias',
    meta: {
        title: 'Cadeterias',
        icon: 'peoples',
        roles: ['admin', 'comercios']
    },
    children: [

        {
            path: 'lista-cadeterias',
            component: () =>
                import ('@/views/abm-cadeterias/complex-table'),
            name: 'Cadeterias',
            meta: { title: 'Cadeterias' }
        }
    ]
}
export default pacientesRouter