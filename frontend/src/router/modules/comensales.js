/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/layout'

const pacientesRouter = {
    path: '/comensales',
    component: Layout,
    redirect: '/abm-comensales/complex-table',
    name: 'Comensales',
    meta: {
        title: 'Comensales',
        icon: 'peoples',
        roles: ['admin']
    },
    children: [

        {
            path: 'lista-comensales',
            component: () =>
                import ('@/views/abm-comensales/complex-table'),
            name: 'Comensales',
            meta: { title: 'Comensales' }
        }
    ]
}

export default pacientesRouter