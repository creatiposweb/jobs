/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/layout'

const pacientesRouter = {
  path: '/comercios',
  component: Layout,
  redirect: '/abm-comercios/complex-table',
  name: 'Comercios',
  meta: {
    title: 'Comercios',
    icon: 'shopping',
    roles: ['admin']
  },
  children: [

    {
      path: 'lista-comercios',
      component: () => import('@/views/abm-comercios/complex-table'),
      name: 'Comercios',
      meta: { title: 'Comercios' }
    }
  ]
}
export default pacientesRouter
