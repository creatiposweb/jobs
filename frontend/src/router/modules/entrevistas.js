/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/layout'

const pacientesRouter = {
    path: '/entrevistas',
    component: Layout,
    redirect: '/abm-entrevistas/complex-table',
    name: 'Entrevistas',
    meta: {
        title: 'Entrevistas',
        icon: 'calendar',
        roles: ['admin']
    },
    children: [

        {
            path: 'lista-entrevistas',
            component: () =>
                import ('@/views/abm-entrevistas/complex-table'),
            name: 'Entrevistas',
            meta: { title: 'Entrevistas' }
        }
    ]
}

export default pacientesRouter