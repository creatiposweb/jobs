/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/layout'

const pacientesRouter = {
    path: '/menu',
    component: Layout,
    redirect: '/abm-menu/complex-table',
    name: 'menu',
    meta: {
        title: 'menu',
        icon: 'peoples',
        roles: ['admin', 'comercios']
    },
    children: [

        {
            path: 'lista-menu',
            component: () =>
                import ('@/views/abm-menu/complex-table'),
            name: 'menu',
            meta: { title: 'Menu' }
        }
    ]
}
export default pacientesRouter