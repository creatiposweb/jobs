/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/layout'

const pacientesRouter = {
    path: '/ordenes',
    component: Layout,
    redirect: '/abm-ordenes/complex-table',
    name: 'ordenes',
    meta: {
        title: 'ordenes',
        icon: 'peoples',
        roles: ['admin', 'comercios']
    },
    children: [

        {
            path: 'lista-ordenes',
            component: () =>
                import ('@/views/abm-ordenes/complex-table'),
            name: 'ordenes',
            meta: { title: 'Ordenes' }
        }
    ]
}
export default pacientesRouter