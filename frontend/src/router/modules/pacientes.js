/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/layout'

const pacientesRouter = {
  path: '/pacientes',
  component: Layout,
  redirect: '/pacientes/complex-table',
  name: 'Pacientes',
  meta: {
    title: 'Pacientes',
    icon: 'peoples'
  },
  children: [

    {
      path: 'lista-pacientes',
      component: () => import('@/views/pacientes/complex-table'),
      name: 'Lista Pacientes',
      meta: { title: 'Lista Pacientes' }
    },
        {
      path: 'alta-pacientes',
      component: () => import('@/views/pacientes/complex-table'),
      name: 'Alta Pacientes',
      meta: { title: 'Alta Pacientes' }
    }
  ]
}
export default pacientesRouter
