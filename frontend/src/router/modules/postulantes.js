/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/layout'

const pacientesRouter = {
    path: '/postulantes',
    component: Layout,
    redirect: '/abm-postulantes/complex-table',
    name: 'Postulantes',
    meta: {
        title: 'Postulantes',
        icon: 'peoples',
        roles: ['admin']
    },
    children: [

        {
            path: 'lista-postulantes',
            component: () =>
                import ('@/views/abm-postulantes/complex-table'),
            name: 'Postulantes',
            meta: { title: 'Postulantes' }
        }
    ]
}

export default pacientesRouter