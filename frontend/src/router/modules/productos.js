/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/layout'

const productosRouter = {
  path: '/productos',
  component: Layout,
  redirect: '/productos/complex-table',
  name: 'Productos',
  meta: {
    title: 'Pacientes',
    icon: 'peoples'
  },
  children: [
    {
      path: 'dynamic-table',
      component: () => import('@/views/productos/dynamic-table/index'),
      name: 'productos DynamicTable',
      meta: { title: 'productos Dynamic Table' }
    },
    {
      path: 'drag-table',
      component: () => import('@/views/productos/drag-table'),
      name: 'productos DragTable',
      meta: { title: 'productos Drag Table' }
    },
    {
      path: 'inline-edit-table',
      component: () => import('@/views/productos/inline-edit-table'),
      name: 'productos InlineEditTable',
      meta: { title: 'productos Inline Edit' }
    },
    {
      path: 'complex-table',
      component: () => import('@/views/productos/complex-table'),
      name: 'productos ComplexTable',
      meta: { title: 'productos Complex Table' }
    }
  ]
}
export default productosRouter
