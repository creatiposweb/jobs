/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/layout'

const pacientesRouter = {
    path: '/usuarios',
    component: Layout,
    redirect: '/abm-comensales/complex-table',
    name: 'usuarios',
    meta: {
        title: 'Usuarios',
        icon: 'peoples',
        roles: ['admin', 'comercios']
    },
    children: [

        {
            path: 'lista-usuarios',
            component: () =>
                import ('@/views/abm-comensales/complex-table'),
            name: 'Usuarios',
            meta: { title: 'Usuarios' }
        }
    ]
}
export default pacientesRouter