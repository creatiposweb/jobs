/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/layout'

const pacientesRouter = {
    path: '/vacantes',
    component: Layout,
    redirect: '/abm-vacantes/complex-table',
    name: 'Vacantes',
    meta: {
        title: 'Vacantes',
        icon: 'suite',
        roles: ['admin']
    },
    children: [

        {
            path: 'lista-vacantes',
            component: () =>
                import ('@/views/abm-vacantes/complex-table'),
            name: 'Vacantes',
            meta: { title: 'Vacantes' }
        }
    ]
}

export default pacientesRouter