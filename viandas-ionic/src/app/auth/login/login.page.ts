import { Component, OnInit } from '@angular/core';
import { ModalController, NavController } from '@ionic/angular';
import { RegisterPage } from '../register/register.page';
import { NgForm } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { AlertService } from 'src/app/services/alert.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  
  data :any;
  constructor(
    private router: Router,
    private modalController: ModalController,
    private authService: AuthService,
    private navCtrl: NavController,
    private alertService: AlertService
  ) { }
  ngOnInit() {
   // this.ionViewWillEnter();
  }
  // Dismiss Login Modal
  dismissLogin() {
    this.modalController.dismiss();
  }
  // On Register button tap, dismiss login modal and open register modal
  async registerModal() {
    this.dismissLogin();
    const registerModal = await this.modalController.create({
      component: RegisterPage
    });
    return await registerModal.present();
  }
  login(form: NgForm) {
    this.authService.login(form.value.email, form.value.password).subscribe(
      data => {
        this.data = data;
        this.alertService.presentToast("Logged In "+ this.data.data.token);

        if(this.data.data.token != null ){

          //this.navCtrl.navigateRoot('/tabs');

        }
        
      },
      error => {
        console.log(error);
      },
      () => {
        //this.dismissLogin();
       console.log("hola");
       this.router.navigate(['/tabs']);

      }
    );
  }

  ionViewWillEnter() {
    
    this.authService.getToken().then(() => {
     // console.log("ghfg");
      if(this.authService.isLoggedIn) {
        console.log("Login In");
        //this.navCtrl.navigateRoot('/tabs');
      }
    });
  }

}