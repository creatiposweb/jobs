import { Component,OnInit } from '@angular/core';
import { ApiService } from "../services/api.service";
import { AlertController } from '@ionic/angular';
@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
  items:any;
  selectedRadioGroup:any;
  selectedRadioGroupname:any;
  pedirboton:any;
  url = "http://viandas.creatipos.com.ar/api/menu/day";
  constructor(private api: ApiService, public alertController: AlertController) {

       this.menu();

  }

  async presentAlertConfirm() {
    const alert = await this.alertController.create({
      header: 'Confirmar!',
      message: 'Va a pedir <strong>' + this.selectedRadioGroupname  + '</strong>!!!',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Okay',
          handler: () => {
            console.log('Confirm Okay');
            this.submitForm();
          }
        }
      ]
    });

    await alert.present();
  }

  radioGroupChange(event) {
    console.log("radioGroupChange",event.detail);
    this.selectedRadioGroup = event.detail;

    for (let item of this.items)
    {

      if(event.detail.value == item.id){

        this.selectedRadioGroupname = item.nombre;

        console.log("radioGroupChange",item.nombre);

      }

      

    }

    if( event.detail.value == null ){ 
    this.pedirboton = true;

    }else{

      this.pedirboton = false;
    }
  }

  public submitForm(){
    
    console.log("radioGroupChange",this.selectedRadioGroup.value);
  }


  public menu(){
    
    this.api
      .getListOfGroup(this.url)
      .subscribe(
        data => {
          console.log(data.items.data);
          this.items =data.items.data;
        },
        err => {
          console.log(err);
        }
      );
    
  }



  // sendPostRequest() {
  //   var headers = new Headers();
  //   headers.append("Accept", 'application/json');
  //   headers.append('Content-Type', 'application/json' );
  //   const requestOptions = new RequestOptions({ headers: headers });

  //   let postData = {
  //           "name": "Customer004",
  //           "email": "customer004@email.com",
  //           "tel": "0000252525"
  //   }

  //   this.http.post("http://127.0.0.1:3000/customers", postData, requestOptions)
  //     .subscribe(data => {
  //       console.log(data['_body']);
  //      }, error => {
  //       console.log(error);
  //     });
  // }

}
