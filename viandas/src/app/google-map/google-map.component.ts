import { Component, ViewChild} from '@angular/core';
import { google } from "google-maps";



@Component({
  selector: 'app-google-map',
  templateUrl: './google-map.component.html',
  styleUrls: ['./google-map.component.scss'],
})


export class GoogleMapComponent {
  google : google;

  @ViewChild('map',{static: true}) mapElement;

  map:any;

  GoogleAutocomplete = new google.maps.places.AutocompleteService();
  puntoa = { input: '' };
  puntob = { input: '' };
  autocompleteItems = [];
  autocompleteItemsb = [];
  geocoder = new google.maps.Geocoder;
  markers = [];
  directionsService = new google.maps.DirectionsService;
  directionsDisplay = new google.maps.DirectionsRenderer;
  start ;
  end = 'chicago, il';
  pos:any;
  lat:any;
  lng:any;
  public distancia:any;
  public monto:any;
  constructor() { }

  ngOnInit(){

    this.initMap();
    console.log("afterinit");
    console.log(this.mapElement.nativeElement);   
   }
   initMap(){
    
    let coord = new google.maps.LatLng(-30.945171400000003,-61.5607353)
    let mapOptions:google.maps.MapOptions = {
      center: coord,
      zoom:16,
      mapTypeControl: false,
      mapTypeId: google.maps.MapTypeId.ROADMAP    
    
    }
    this.map = new google.maps.Map(this.mapElement.nativeElement ,mapOptions)
    this.directionsDisplay.setMap(this.map);

    navigator.geolocation.getCurrentPosition( pos => {
      this.lng = +pos.coords.longitude;
      this.lat = +pos.coords.latitude;
    });
    
    console.log( this.lng );
    //this.map.setCenter(this.pos);

   }
   
   //Actualizamos los resultados de busqueda punta A.
   updateSearchResults(){

    if (this.puntoa.input == '') {
      this.autocompleteItems = [];
      return;
    }

    this.GoogleAutocomplete.getPlacePredictions({ input: this.puntoa.input },
    (predictions, status) => {
      this.autocompleteItems = [];

        predictions.forEach((prediction) => {
          this.autocompleteItems.push(prediction);
        });
    


    });

  }
//Fin de

//Actualizamos los resultados de busqueda punta A.
updateSearchResultsb(){

  if (this.puntob.input == '') {
    this.autocompleteItemsb = [];
    return;
  }
  
  this.GoogleAutocomplete.getPlacePredictions({ input: this.puntob.input },
  (predictions, status) => {
    this.autocompleteItemsb = [];

      predictions.forEach((prediction) => {
        this.autocompleteItemsb.push(prediction);
      });
  


  });

}
//Fin de

  selectSearchResult(item){
    
    this.autocompleteItems = [];
    this.puntoa.input  ="";
    this.geocoder.geocode({'placeId': item.place_id}, (results, status) => {
      if(results[0]){
        let position = {
            lat: results[0].geometry.location.lat,
            lng: results[0].geometry.location.lng
        };
        let marker = new google.maps.Marker({
          position: results[0].geometry.location,
          map: this.map,
        });
        this.puntoa.input  = item.description;
        this.start = results[0].geometry.location;
        
        //this.markers.push(marker);
        //this.map.setCenter(results[0].geometry.location);
      }
    })

  }


  selectSearchResultb(item){
    
    this.autocompleteItemsb = [];
    this.puntob.input  ="";
    this.geocoder.geocode({'placeId': item.place_id}, (results, status) => {
      if(results[0]){
        let position = {
            lat: results[0].geometry.location.lat,
            lng: results[0].geometry.location.lng
        };
        let marker = new google.maps.Marker({
          position: results[0].geometry.location,
          map: this.map,
        });
        //this.markers.push(marker);
        //this.map.setCenter(results[0].geometry.location);
        console.log(item.description);
        this.puntob.input  = item.description;
        this.calculateAndDisplayRoute(results[0].geometry.location);
      }
    })

  }

 calculateAndDisplayRoute(end) {
  
    this.directionsService.route({
      origin: this.start,
      destination: end,
      //travelMode: "DRIVING"
    }, (response, status) => {
      if (status ) {
        // Aqui con el response podemos acceder a la distancia como texto 
        console.log(response.routes[0].legs[0].distance.text);
        this.distancia = response.routes[0].legs[0].distance.text;

        //regla de tres
             var calculo = response.routes[0].legs[0].distance.value * 40 ;
             calculo = calculo / 1000;
        this.monto =calculo;
        // Obtenemos la distancia como valor numerico en metros 
       console.log(response.routes[0].legs[0]);

        this.directionsDisplay.setDirections(response);
        //window.alert('Directions request failed due to ' + status);
      } else {
        window.alert('Directions request failed due to ' + status);
      }
    });

    
  }


}
