import { Component } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { ModalController, MenuController, NavController } from '@ionic/angular';
@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {

  constructor(

    private authService: AuthService,
    private navCtrl: NavController,
  ) { 
    //this.menu.enable(false);
  }


  ionViewWillEnter() {
    this.authService.getToken().then(() => {
      if(this.authService.isLoggedIn) {
        this.navCtrl.navigateRoot('/login');
      }
    });
  }


}
